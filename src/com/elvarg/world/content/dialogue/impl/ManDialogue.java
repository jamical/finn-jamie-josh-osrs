package com.elvarg.world.content.dialogue.impl;

import com.elvarg.world.content.dialogue.Dialogue;
import com.elvarg.world.entity.impl.npc.NPC;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Node;

/**
 * A dialogue that belongs to the npc of Man.
 * First dialogue made with new system. Testing it out.
 * @author jamix77
 *
 */
public class ManDialogue extends Dialogue {

	/**
	 * Our default constructor with player inside :P
	 * @param player the player.
	 */
	public ManDialogue(Player player) {
		super(player);
	}

	/**
	 * Handle our dialogue
	 */
	@Override
	protected boolean handle(int option) {
		switch (stage) {
		case 0:
			interpreter.npc("Hello dude!");
			break;
		case 1:
			interpreter.player("Hi");
			break;
		case 2:
			interpreter.options("Op1","op2");
			break;
		case 3:
			switch (option) {
			case -1:
				stage = 1;
				return false;
			}
			player.getPacketSender().sendInterfaceRemoval()
			.sendMessage(option == 1 ? "Spaghetti meatballs":"Steak");
			break;
		case 4:
			return true;
		}
		return false;
	}

	/**
	 * Nodes that initlize the {@code ManDialogue} dialogue.
	 */
	@Override
	public Node[] nodes() {
		return new Node[] {new NPC(385,null)};
	}

}
