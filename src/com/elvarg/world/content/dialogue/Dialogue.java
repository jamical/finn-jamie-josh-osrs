package com.elvarg.world.content.dialogue;

import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Node;

/**
 * A peice of dialogue. this is the abstract dialogue so this is the class which 
 * will be extended into other dialogue classes.
 * @author jamix77
 *
 */
public abstract class Dialogue {
	
	/**
	 * This is the player who is in the dialogue with (whoever).
	 */
	protected final Player player;
	
	/**
	 * Our dialogue stage. Used for the handling of the dialogue. Not always used but best way to do this.
	 */
	protected int stage;
	
	/**
	 * Our node in which we are talking to.
	 */
	private Node node;
	
	/**
	 * The interpreter for the dialogue at this moment.
	 */
	protected DialogueInterpreter interpreter;
	
	/**
	 * Constructor to set our player.
	 * @param player The player.
	 */
	public Dialogue(final Player player) {
		this.player = player;
		interpreter = new DialogueInterpreter(this.player,this);
	}
	
	/**
	 * Handle our dialogue. 
	 * @return {@code True} If we should quit.
	 * @return {@code False} If we should continue.
	 */
	protected abstract boolean handle(int option);
	
	/**
	 * Defaultly handle our dialogue here. Then call the abstract dialogue. This one contains no option so it does the
	 * other method also called defaultHandle but it supplys that one with the argument of -1. Indicating that there
	 * is no button id chosen.
	 */
	public void defaultHandle() {
		defaultHandle(-1);
	}
	
	/**
	 * Defaultly handle our dialogue here. Then call the abstract dialogue.
	 */
	public void defaultHandle(int option) {
		if (handle(option)) {
			end();
		}
		stage++;
	}
	
	/**
	 * Initilize the dialogue.
	 */
	public void init() {
		player.setNewDialogue(this);
		defaultHandle();
	}
	
	/**
	 * End our dialogue session and resume back to whatever we were doing previously.
	 */
	private void end() {
		player.setNewDialogue(null);
	}
	
	/**
	 * The nodes that can initilize a dialogue.
	 * @return
	 */
	public abstract Node[] nodes();

	public Node getNode() {
		return node;
	}

	public void setNode(Node node) {
		this.node = node;
	}

}
