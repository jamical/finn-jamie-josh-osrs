package com.elvarg.world.content.dialogue;

import com.elvarg.cache.impl.definitions.NpcDefinition;
import com.elvarg.world.entity.impl.npc.NPC;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Item;

/**
 * The interpreter for dialogue system.
 * @author jamix77
 *
 */
public class DialogueInterpreter {
	
	/**
	 * Player who owns the interpreter at this current moment.
	 */
	protected final Player player;
	
	/**
	 * The dialogue that we are interpreting and sending to the player
	 */
	protected final Dialogue dialogue;
	
	/**
	 * Constructor with player inside to indicate which player is owning the interpreter at this current moment.
	 * @param player The player.
	 */
	public DialogueInterpreter(final Player player,final Dialogue dialogue) {
		this.player = player;
		this.dialogue = dialogue;
	}
	
	/**
	 * Send an npc dialogue.
	 * @param lines the lines of dialogue
	 * @return the interpreter instance.
	 */
	public DialogueInterpreter npc(String...lines) {
		if (!(dialogue.getNode() instanceof NPC)) {
			return this;//cant continue because the node is not an npc.
		}
		return npc((NPC)dialogue.getNode(),Expression.DEFAULT,lines);
	}
	
	/**
	 * Sends an npc dialogue. This is with more detail.
	 * @param npc the npc.
	 * @param e the expression on the npcs face.
	 * @param lines the lines of dialogue.
	 * @return the interpreter instance.
	 */
	public DialogueInterpreter npc(NPC npc,Expression e,String...lines) {
		int startDialogueChildId = NODE_DIALOGUE_ID[lines.length - 1];
		int headChildId = startDialogueChildId - 2;
		player.getPacketSender().sendNpcHeadOnInterface(npc.getId(), headChildId);
		player.getPacketSender().sendInterfaceAnimation(headChildId, e.getAnimation());
		player.getPacketSender().sendString(startDialogueChildId - 1, NpcDefinition.forId(npc.getId()) != null ? NpcDefinition.forId(npc.getId()).getName().replaceAll("_", " ") : "");
		for (int i = 0; i < lines.length; i++) {
			player.getPacketSender().sendString(startDialogueChildId + i, lines[i]);
		}
		player.getPacketSender().sendChatboxInterface(startDialogueChildId - 3);
		return this;
	}
	
	/**
	 * Sends a player dialogue.
	 * @param lines the lines of dialogue
	 * @return the interpreter instance.
	 */
	public DialogueInterpreter player(String...lines) {
		return player(Expression.DEFAULT,lines);
	}
	
	/**
	 * Sends a player dialogue. With more detail on what to do.
	 * @param e the expression of the dialogue speaker.
	 * @param lines the lines of the dialogue.
	 * @return the interpreter instance.
	 */
	public DialogueInterpreter player(Expression e, String...lines) {
		int startDialogueChildId = PLAYER_DIALOGUE_ID[lines.length - 1];
		int headChildId = startDialogueChildId - 2;
		player.getPacketSender().sendPlayerHeadOnInterface(headChildId);
		player.getPacketSender().sendInterfaceAnimation(headChildId, e.getAnimation());
		player.getPacketSender().sendString(startDialogueChildId - 1, player.getUsername());
		for (int i = 0; i < lines.length; i++) {
			player.getPacketSender().sendString(startDialogueChildId + i, lines[i]);
		}
		player.getPacketSender().sendChatboxInterface(startDialogueChildId - 3);
		return this;
	}
	
	/**
	 * Sends an item dialogue.
	 * @param lines the lines of dialogue.
	 * @return the interpreter instance.
	 */
	public DialogueInterpreter item(String...lines) {
		if (!(dialogue.getNode() instanceof Item)) {
			return this;//cannot continue with the dialogue because not isnt an instance of item. to send an item send the other method that asks you to specify an item.
		}
		return item((Item)dialogue.getNode(),lines);
	}
	
	/**
	 * Sends an item dialogue. With more detail.
	 * @param item the item to send.
	 * @param lines the lines of dialogue.
	 * @return the interpreter instance.
	 */
	public DialogueInterpreter item(Item item, String...lines) {
		int startDialogueChildId = NODE_DIALOGUE_ID[lines.length - 1];
		int headChildId = startDialogueChildId - 2;
		player.getPacketSender().sendInterfaceModel(headChildId, item.getId(), 150);
		player.getPacketSender().sendString(startDialogueChildId - 1, item.getDefinition() == null ? "?" : item.getDefinition().getName() == null ? "?" : item.getDefinition().getName());
		for (int i = 0; i < lines.length; i++) {
			player.getPacketSender().sendString(startDialogueChildId + i, lines[i]);
		}
		player.getPacketSender().sendChatboxInterface(startDialogueChildId - 3);
		return this;
	}
	
	/**
	 * Sends a set of options.
	 * @param lines
	 * @return
	 */
	public DialogueInterpreter options(String...lines) {
		int firstChildId = OPTION_DIALOGUE_ID[lines.length - 1];
		player.getPacketSender().sendString(firstChildId - 1, "Choose an option");
		for (int i = 0; i < lines.length; i++) {
			player.getPacketSender().sendString(firstChildId + i, lines[i]);
		}
		player.getPacketSender().sendChatboxInterface(firstChildId - 2);
		return this;
	}
	
	
	
	/**
	 * This array contains the child id where the dialogue
	 * statement starts for npc and item dialogues.
	 */
	private static final int[] NODE_DIALOGUE_ID = {
		4885,
		4890,
		4896,
		4903
	};
	
	/**
	 * This array contains the child id where the dialogue
	 * statement starts for player dialogues.
	 */
	private static final int[] PLAYER_DIALOGUE_ID = {
		971,
		976,
		982,
		989
	};
	
	/**
	 * This array contains the child id where the dialogue
	 * statement starts for option dialogues.
	 */
	private static final int[] OPTION_DIALOGUE_ID = {
		13760,
		2461,
		2471,
		2482,
		2494,
	};

}
