package com.elvarg.world.content.sounds;
/**
 * Types of sounds.
 * @author jamix77
 *
 */
public enum SoundType {

	/**
	 * This can be regional music or just normal music.
	 */
	MUSIC,
	
	/**
	 * This can be any type of ingame sound.
	 */
	GAMESOUNDS;
	
}
