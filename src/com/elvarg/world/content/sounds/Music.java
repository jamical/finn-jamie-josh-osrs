package com.elvarg.world.content.sounds;

import com.elvarg.world.entity.impl.player.Player;

/**
 * In game music. Wheather it be regional or it be just a song played by the player from the song book.
 * @author jamix77
 *
 */
public class Music extends DefaultSound {

	/**
	 * Constructor with music id inside.
	 * @param id
	 */
	public Music(int id) {
		super(id);
	}
	
	/**
	 * Create our music thorugh a static method.
	 * @param player
	 * @param id
	 * @return
	 */
	public static Music create(Player player, int id) {
		Music m = new Music(id);
		m.setPlayer(player);
		return m;
	}

	
	/**
	 * What type of sound is this?
	 */
	@Override
	public SoundType type() {
		return SoundType.MUSIC;
	}

}
