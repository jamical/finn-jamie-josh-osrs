package com.elvarg.world.content.sounds;

import com.elvarg.world.entity.impl.player.Player;

/**
 * Ingame sound.
 * @author jamix77
 *
 */
public class Sound extends DefaultSound {

	/**
	 * Default constructor with id inside.
	 * @param id
	 */
	public Sound(int id) {
		super(id);
	}
	
	/**
	 * Create our sound.
	 * @param player
	 * @param id
	 * @return
	 */
	public static Sound create(Player player,int id) {
		Sound s = new Sound(id);
		s.setPlayer(player);
		return s;
	}

	/**
	 * What type of sound is this?
	 */
	@Override
	public SoundType type() {
		return SoundType.GAMESOUNDS;
	}

}
