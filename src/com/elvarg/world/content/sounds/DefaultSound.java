package com.elvarg.world.content.sounds;

import com.elvarg.world.entity.impl.player.Player;

/**
 * The default sound.
 * @author jamix77
 *
 */
public abstract class DefaultSound {

	/**
	 * The id of the sound.
	 */
	private int id;
	
	/**
	 * Player who owns this sound.
	 */
	private Player player;
	
	/**
	 * Default constructor with the sounds id.
	 * @param id
	 */
	public DefaultSound(int id) {
		this.setId(id);//the sounds id.
	}
	
	/**
	 * Play our music / sounds
	 */
	public void play() {
		switch (type()) {
		case GAMESOUNDS:
			getPlayer().getPacketSender().sendSound((Sound)this);
			break;
		case MUSIC:
			getPlayer().getPacketSender().sendSong((Music)this);
			break;
		default:
			getPlayer().getPacketSender().sendMessage("This type of sound is invalid. Send this to an administrator. " + type().name() + " " + type().hashCode());
			break;
		
		}
	}
	
	/**
	 * What type of sound is this?
	 * @return
	 */
	public abstract SoundType type();

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
