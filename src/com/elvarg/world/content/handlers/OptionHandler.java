package com.elvarg.world.content.handlers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.elvarg.world.entity.impl.npc.NPC;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Node;

/**
 * An abstract option handler.
 * @author jamix77
 *
 */
public abstract class OptionHandler {
	
	public static List<Node> nodes = new ArrayList<>();
	
	public static List<Integer> options = new ArrayList<>();
	
	public static List<OptionHandler> handlers = new ArrayList<>();
	
	/**
	 * Assign one.
	 * @param node
	 * @param option
	 * @param handler
	 */
	public static void assign(Node node, int option, OptionHandler handler) {
		nodes.add(node);
		options.add(option);
		handlers.add(handler);
	}
	
	/**
	 * Get an OptionHandler
	 * @param node the node.
	 * @param option the option.
	 * @return A null or optionHandler.
	 */
	public static OptionHandler get(Node node,int option) {
		for (int i = 0; i < nodes.size(); ++i) {
			//if the nodes are the same type of nodes. Like item or gameobject.
			
			//Cluncky design which can be shorternd. Do later TODO
			if (nodes.get(i) != null) {
				if (nodes.get(i).type() == node.type()) {
					switch (nodes.get(i).type()) {
					case GameObject:
						break;
					case Item:
						break;
					case Npc:
						NPC nodeAsNpc = (NPC) node;
						NPC nodesListAsNpc = (NPC) nodes.get(i);
						if (nodeAsNpc.getId() == nodesListAsNpc.getId()) {
							if (options.get(i) != null) {
								if ((int)option == (int)options.get(i)) {
									if (handlers.get(i) != null) { 
										return handlers.get(i);
									}
								}
							}
						}
						break;
					case Null:
						break;
					case Player:
						break;
					default:
						break;
					
					}
					
				}
			}
		}
		return null;
	}
	
	/**
	 * Initilise this at the start of the game or in the middle to hotfix.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	public static void init() throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		BufferedReader reader = new BufferedReader(new FileReader(new File("data/ophandlers")));
		String read = "";
		while ((read = reader.readLine()) != null) {
			Class c = Class.forName(read);
			if (c != null) {
				OptionHandler optionHan = (OptionHandler) c.newInstance();
				optionHan.initAssign();
			}
		}
	}
	
	/**
	 * Initilise all our asigns for the handler. TODO
	 */
	public abstract  void initAssign();
	
	/**
	 * Handle an action.
	 * @param node
	 */
	public abstract void handle(Player player,Node node);
	
	
}
