package com.elvarg.world.content.skills.impl.firemaking;

import java.util.HashMap;
import java.util.Map;

/**
 * Firemaking data.
 * @author jamix77
 *
 */
public enum LogData {

	LOG(1511, 1, 40, 26185,180),
	Achey_LOG(2862, 1, 40, 26185,180),
	OAK_LOG(1521, 15, 60, 26185,200),
	WILLOW_LOG(1519, 30, 90, 26185,300),
	TEAK_LOG(6333, 35, 105, 26185,450),
	ARCTIC_PINE_LOG(10810, 42, 125, 26185,500),
	MAPLE_LOG(1517, 45, 135, 26185,400),
	MAHOGANY_LOG(6332, 50, 157.5, 26185,400),
	YEW_LOG(1515, 60, 202.5, 26185,202.5),
	MAGIC_LOG(1513, 75, 303.8, 26185,303.8),
	BLUE_LOG(7406, 1, 250, 26576,180),
	GREEN_LOG(7405, 1, 250, 26575,180),
	RED_LOG(7404, 1, 250, 26186,180),
	WHITE_LOG(10328, 1, 250, 20000,180),
	PURPLE_LOG(10329, 1, 250, 20001,180);

	private int log;
	private int level;
	private double exp;
	private int fire;
	private double life;

	private LogData(int log, int level, double exp, int fire,double life) {
		this.log = log;
		this.level = level;
		this.exp = exp;
		this.fire = fire;
		this.life = life;
	}
	
	/**
	 * The log data in a hashmap.
	 */
	private static Map<Integer,LogData> DATA = new HashMap<Integer,LogData>();
	
	static {
		for (LogData d : values()) {
			DATA.put(d.getLog(), d);
		}
	}
	
	public static LogData forId(int logId) {
		return DATA.get(logId);
	}

	public double getLife() {
		return life;
	}
	
	public void setLife(double life) {
		this.life = life;
	}
	
	public int getLog() {
		return log;
	}

	public int getLevel() {
		return level;
	}

	public double getExperience() {
		return exp;
	}
	
	public int getFire() {
		return fire;
	}

	public static Map<Integer,LogData> getDATA() {
		return DATA;
	}

	public static void setDATA(Map<Integer,LogData> dATA) {
		DATA = dATA;
	}

}