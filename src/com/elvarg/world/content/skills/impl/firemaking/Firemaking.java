package com.elvarg.world.content.skills.impl.firemaking;

import com.elvarg.world.collision.region.RegionClipping;
import com.elvarg.world.content.skills.AbstractSkill;
import com.elvarg.world.content.skills.InteractionType;
import com.elvarg.world.content.sounds.Sound;
import com.elvarg.world.entity.impl.object.GameObject;
import com.elvarg.world.entity.impl.object.ObjectHandler;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.grounditems.GroundItemManager;
import com.elvarg.world.model.Animation;
import com.elvarg.world.model.GroundItem;
import com.elvarg.world.model.Item;
import com.elvarg.world.model.Locations.Location;
import com.elvarg.world.model.Node;
import com.elvarg.world.model.Skill;

/**
 * Firemaking skill.
 * @author jamix77
 *
 */
public class Firemaking extends AbstractSkill {
	
	public Firemaking() {
		
	}
	
	/**
	 * Lighting fire animation.
	 */
	private final static Animation LIGHT_FIRE = new Animation(733);

	public Firemaking(Player player) {
		super(player);
	}
	
	/**
	 * The log we are lighting
	 */
	private LogData log;
	
	/**
	 * The log that gets layed on the ground.
	 */
	private GroundItem groundlog;
	
	/**
	 * The fire object.
	 */
	private GameObject fire;
	

	@Override
	public void handle() {
		if (ticks == 1) {
			init();
			return;
		}
		if (groundlog == null || groundlog.hasBeenPickedUp()) {
			getPlayer().performAnimation(Animation.DEFAULT_RESET_ANIMATION);
			this.stop();
			return;
		}
		if (ticks == 0) {
			getPlayer().performAnimation(LIGHT_FIRE);
		}
		if (ticks % 3 != 0) {
			return;
		}
		if (ticks % 12 == 0) {
			getPlayer().performAnimation(LIGHT_FIRE);
		}
		Sound.create(player, 375).play();
		if (!success()) {
			return;
		}
		//We did it!
		getPlayer().performAnimation(Animation.DEFAULT_RESET_ANIMATION);
		if (getPlayer().getMovementQueue().canWalk(1, 0)) {
			getPlayer().getMovementQueue().walkStep(1, 0);
		} else {
			if (getPlayer().getMovementQueue().canWalk(-1, 0)) {
				getPlayer().getMovementQueue().walkStep(-1, 0);
			} else {
				if (getPlayer().getMovementQueue().canWalk(0, -1)) {
					getPlayer().getMovementQueue().walkStep(0, -1);
				} else {
					if (getPlayer().getMovementQueue().canWalk(0,1)) {
						getPlayer().getMovementQueue().walkStep(0, 1);
					}
				}
			}
		}
		fire = new GameObject(log.getFire(), getPlayer().getPosition());//makes the fire gameobject something.
		ObjectHandler.spawnGlobalObject(fire);
		GroundItemManager.remove(groundlog, true);
		getPlayer().setPositionToFace(fire.getPosition());
		getPlayer().getSkillManager().addExperience(Skill.FIREMAKING, (int)log.getExperience() * 6);
		getPlayer().getPacketSender().sendMessage("You light the log.");
		super.stop();
		return;
	}
	
	/**
	 * Checks if the lighting was successful
	 * @return
	 */
	private boolean success() {
		int level = 1 + getPlayer().getSkillManager().getCurrentLevel(Skill.FIREMAKING);
		double hostRatio = Math.random() * log.getLevel();
		double clientRatio = Math.random() * ((level - log.getLevel()) * (1 + (log.getLevel() * 0.01)));
		return hostRatio < clientRatio && !groundlog.hasBeenPickedUp();
	}
	
	/**
	 * Init method.
	 */
	private void init() {
		log = LogData.forId(((Item)super.getArgs()[0]).getId());
		if (log == null) {
			log = LogData.forId(((Item)super.getArgs()[1]).getId());
			if (log == null) {
				super.stop();
				return;
			}
		}
		if (getPlayer().getSkillManager().getCurrentLevel(Skill.FIREMAKING) < log.getLevel()) {
			getPlayer().getPacketSender().sendMessage("You need a firemaking level of at least " + log.getLevel() + " to light this log.");
			stop();
			return;
		}
		if (RegionClipping.blocked(getPlayer().getPosition())) {
			getPlayer().getPacketSender().sendMessage("You cannot light a fire here.");
			stop();
			return;
		}
		if (RegionClipping.getGameObject(getPlayer().getPosition()) != null) {
			for (LogData ld : LogData.values()) {
				if (RegionClipping.getGameObject(getPlayer().getPosition()).getId() == ld.getFire()) {
					getPlayer().getPacketSender().sendMessage("You cannot light a fire here.");
					stop();
					return;
				}
			}
		}
		if (getPlayer().getLocation().equals(Location.BANK)) {
			getPlayer().getPacketSender().sendMessage("You cannot light a fire in a bank.");
			stop();
			return;
		}
		groundlog = new GroundItem(new Item(log.getLog()), getPlayer().getPosition(), getPlayer().getUsername(),getPlayer().getHostAddress(),false,5000,false,5000);
		getPlayer().getInventory().delete(log.getLog(),1);
		GroundItemManager.add(groundlog, true);
		getPlayer().performAnimation(LIGHT_FIRE);
		getPlayer().getPacketSender().sendMessage("You attempt to light the logs..");
		Sound.create(player, 375);
	}

	@Override
	public InteractionType interactionType() {
		return InteractionType.ITEM_ON_ITEM;
	}

	@Override
	public Node[][] nodes() {
		return new Node[][] {{new Item(590,1)},{new Item(1511,1)}};
	}
}
