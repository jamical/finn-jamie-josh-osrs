package com.elvarg.world.content.skills.impl.prayer;

import com.elvarg.engine.task.Task;
import com.elvarg.engine.task.TaskManager;
import com.elvarg.world.content.skills.AbstractSkill;
import com.elvarg.world.content.skills.InteractionType;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Animation;
import com.elvarg.world.model.Item;
import com.elvarg.world.model.Node;
import com.elvarg.world.model.Skill;

/**
 * Prayer skill.
 * @author jamix77
 *
 */
public class BoneBuryingPrayer extends AbstractSkill {
	
	private static final Animation BURY = new Animation(827);
	
	/**
	 * Default constructor.
	 */
	public BoneBuryingPrayer() {}
	
	/**
	 * Constructor with player inside.
	 * @param player
	 */
	public BoneBuryingPrayer(Player player) {
		super(player);
	}

	@Override
	public void handle() {
		if (ticks == 1) {
			player.getInventory().delete((Item)getArgs()[0]);
			player.getPacketSender().sendMessage("You dig a hole in the ground...");
			player.performAnimation(BURY);
			TaskManager.submit(new Task(2,player,false) {
				@Override
				protected void execute() {
					player.getSkillManager().addExperience(Skill.PRAYER, PrayerData.forId(((Item)getArgs()[0]).getId()).getXp());
					player.getPacketSender().sendMessage("... you bury the " + PrayerData.forId(((Item)getArgs()[0]).getId()).getName() + ".");
					stop();
					BoneBuryingPrayer.this.stop();
					return;
				}
			});
		}
		return;
	}

	@Override
	public InteractionType interactionType() {
		return InteractionType.ITEM_CLICK_1;
	}

	@Override
	public Node[][] nodes() {
		Node[] n = new Node[PrayerData.DATA.values().size()];
		int done = 0;
		for (PrayerData data : PrayerData.DATA.values()) {
			n[done] = new Item(data.getItemId());
			done++;
		}
		return new Node[][] {n};
	}

}
