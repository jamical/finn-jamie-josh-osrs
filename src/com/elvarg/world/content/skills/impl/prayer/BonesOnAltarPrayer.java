package com.elvarg.world.content.skills.impl.prayer;

import com.elvarg.world.content.skills.AbstractSkill;
import com.elvarg.world.content.skills.InteractionType;
import com.elvarg.world.entity.impl.object.GameObject;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Animation;
import com.elvarg.world.model.Graphic;
import com.elvarg.world.model.Item;
import com.elvarg.world.model.Node;
import com.elvarg.world.model.Skill;

/**
 * Represents the skill of prayer. But this is the prayer of which you will place bones
 * on an altar on 
 * @author jamix77
 *
 */
public class BonesOnAltarPrayer extends AbstractSkill {

	/**
	 * Default constructor.
	 */
	public BonesOnAltarPrayer() {}
	
	/**
	 * Default constructor. With player.
	 * @param player The player.
	 */
	public BonesOnAltarPrayer(Player player) {
		super(player);
	}
	
	/**
	 * Constant of the animation that is used.
	 */
	private static final Animation ANIMATION = new Animation(896);
	
	/**
	 * Constant of the graphic that is used.
	 */
	private static final Graphic GFX = new Graphic(624);
	
	/**
	 * The item id.
	 */
	private int item;
	
	/**
	 * Handle our prayer skill.
	 */
	@Override
	public void handle() {
		if (ticks == 1)
			item = ((Item) getArgs()[0]).getId();//initilize the itemId. Its odd cos when the item is an item instance it will count its slot and check if its null, will depend now on the itemId.
		if (ticks % 5 != 0 && ticks != 1) {
			return;
		}
		if (!player.getInventory().contains(item)) {
			player.getPacketSender().sendMessage("You do not have any bones to submit to the gods.");
			stop();
			return;
		}
		PrayerData da = PrayerData.forId(item);
		player.getInventory().delete(item,1);
		if (da == null) {
			stop();
			return;
		}
		player.setPositionToFace(((GameObject)getArgs()[1]).getPosition());
		player.getSkillManager().addExperience(Skill.PRAYER, da.getXp());
		player.performAnimation(ANIMATION);
		player.getPacketSender().sendGlobalGraphic(GFX, (((GameObject)getArgs()[1]).getPosition()));
	}

	/**
	 * What type of interaction is this?
	 */
	@Override
	public InteractionType interactionType() {
		return InteractionType.ITEM_ON_OBJECT;
	}

	/**
	 * Nodes that trigger this event.
	 * Item first object second.
	 */
	@Override
	public Node[][] nodes() {
		Node[] items = new Node[PrayerData.DATA.values().size()];
		int counter = 0;
		for (PrayerData d : PrayerData.values()) {
			items[counter] = new Item(d.getItemId());
			counter += 1;
		}
		return new Node[][] {items,{new GameObject(409,null)}};
	}

}
