package com.elvarg.world.content.skills.impl.prayer;

import java.util.HashMap;
import java.util.Map;

/**
 * Prayer data.
 * @author jamix77
 *
 */
public enum PrayerData {
	/* Starter Bones */
	REGULAR(526, 50, "Bones"),
	NORMAL2(2530, 50, "Bones"),
	Burnt(528, 50,"Burnt Bones"),
	BATB(530, 50, "Bat Bones"),
	WOLFB(2859, 50,"Wolf Bones"),
	MONKEYB0(3179, 50, "Monkey Bones"),
	MONKEYB1(3180, 50, "Monkey Bones"),
	MONKEYB2(3181, 50, "Monkey Bones"),
	MONKEYB3(3182, 50, "Monkey Bones"),
	MONKEYB4(3183, 50, "Monkey Bones"),
	MONKEYB5(3185, 50, "Monkey Bones"),
	MONKEYB6(3186, 50, "Monkey Bones"),
	MONKEYB7(3187, 50, "Monkey Bones"),
	/* Decent Bones */
	BBONE(532, 70, "Big Bones"), 
	Jogre(3125, 100, "Jogre Bones"),
	BJogre(3127, 110, "Burnt Jogre Bones"),
	Jogre2(3128, 110, "Pasty Jogre Bones"),
	Jogre3(3129, 110, "Pasty Jogre Bones"),
	Jogre4(3130, 110, "Marinated Jogre Bones"),
	Jogre5(3131, 110, "Pasty Jogre Bones"),
	Jogre6(3132, 110, "Pasty Jogre Bones"),
	Jogre7(3133, 110, "Marinated Jogre Bones"),
	Zogre(4812, 110, "Zogre Bones"),
	Shaik(3123, 110, "Shaikahan Bones"), 
	Baby(534, 130,"Baby Dragon Bones"),
	Wyvern(6812, 210, "Wyvern Bones"),
	Dragon(536, 220, "Dragon Bones"), 
	Fayrg(4830, 250, "Fayrg Bones"),
	/* Good Bones */
	OurgB(14793, 270, "Ourg Bones"),
	RaurgBone(4832, 270, "Raurg Bones"), 
	OurgBone(4834, 270, "Ourg Bones"),
	DagBone(6729, 300, "Dagannoth Bones"),
	Curvedb(10977, 500, "Curved bone"),
	Longb(10976, 500, "Long bone");
	
	/**
	 * The name of this type of bone
	 */
	private String name;
	/**
	 * The itemId of the bone.
	 * The xp that the bone gives.
	 */
	private int itemId,xp;
	
	/**
	 * Constructor
	 */
	private PrayerData(int itemId, int xp, String name) {
		this.itemId = itemId;
		this.xp = xp;
		this.name = name;
	}
	
	static final Map<Integer,PrayerData> DATA = new HashMap<Integer,PrayerData>();
	
	static {
		for (PrayerData data : values())
			DATA.put(data.getItemId(), data);
	}
	
	/**
	 * For id method.
	 * @param itemId the item id.
	 * @return the prayerdata instacne.
	 */
	public static PrayerData forId(int itemId) {
		return DATA.get(itemId);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getXp() {
		return xp;
	}

	public void setXp(int xp) {
		this.xp = xp;
	}
	
}
