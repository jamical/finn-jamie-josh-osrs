package com.elvarg.world.content.skills.impl.theiving;

import com.elvarg.GameConstants;
import com.elvarg.cache.impl.definitions.NpcDefinition;
import com.elvarg.util.Misc;
import com.elvarg.world.content.skills.AbstractSkill;
import com.elvarg.world.content.skills.InteractionType;
import com.elvarg.world.entity.impl.npc.NPC;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.grounditems.GroundItemManager;
import com.elvarg.world.model.Animation;
import com.elvarg.world.model.GroundItem;
import com.elvarg.world.model.Item;
import com.elvarg.world.model.Node;
import com.elvarg.world.model.Skill;

/**
 * Theiving skill. This is stealing from an entity.
 * @author jamix77
 *
 */
public class Theiving extends AbstractSkill {
	
	/**
	 * Theiving animation, the picking from the pocket.
	 */
	private static final Animation THEIV_ANIM = new Animation(881);
	
	/**
	 * Our stunning instance.
	 */
	private final Stun stun = new Stun(this);
	
	/**
	 * The pickpocket we are doing.
	 */
	private Pickpocket pick;
	
	/**
	 * The npc who we are theiving from. Poor npc.
	 */
	private NPC npc;
	
	/**
	 * Default constructor
	 */
	public Theiving() {}
	
	/**
	 * Constructor containing player.
	 * @param player
	 */
	public Theiving(Player player) {
		super(player);
	}

	/**
	 * Handle our theiving skill.
	 */
	@Override
	public void handle() {
		if (player.getSkillManager().getCurrentLevel(Skill.THIEVING) < pick.getRequirement()) {
			player.getPacketSender().sendMessage("You need a theiving level of " + pick.getRequirement() + " to theive from this npc.");
			stop();
			return;
		}
		if (ticks == 1) {
			init();
			return;
		}
		if (ticks == 4) {
			if (!stun.success()) {
				stun.stun();
			} else {
				reward();
			}
			stop();
			return;
		}
	}
	
	/**
	 * Reward our player for being able to steal from the npc successfully.
	 */
	public void reward() {
		Item i = Misc.randomElement(pick.getLoot());
		if (i != null) {
			if (i.getAmount() > 0) {
				if (player.getInventory().isFull()) {
					GroundItemManager.add(new GroundItem(i,player.getPosition().copy(),player.getHostAddress(),true,200,true,200), true);
				} else {
					player.getInventory().add(i);
				}
			}
		}
		player.getSkillManager().addExperience(Skill.THIEVING, pick.getExperience()*GameConstants.MULTIPLIER);
	}
	
	/**
	 * Initlize our skill from the first tick.
	 */
	private void init() {
		player.performAnimation(THEIV_ANIM);
		if (NpcDefinition.forId(pick.getNpcId()) != null)
			player.getPacketSender().sendMessage("You attempt to steal from the " + NpcDefinition.forId(pick.getNpcId()).getName() + "'s pockets.");
	}

	@Override
	public InteractionType interactionType() {
		return InteractionType.NPC_OPTION_2;
	}

	@Override
	public Node[][] nodes() {
		return new Node[][]{};
	}
	
	public void setPick(Pickpocket pick) {
		this.pick = pick;
	}
	
	public Pickpocket getPick() {
		return pick;
	}

	public NPC getNpc() {
		return npc;
	}

	public void setNpc(NPC npc) {
		this.npc = npc;
	}

}
