package com.elvarg.world.content.skills.impl.theiving;

import java.util.HashMap;
import java.util.Map;

import com.elvarg.world.model.Item;

/**
 * This enum stores all our thieving pickpocket data.
 * @author Clive/Dinh
 * @author jamix77
 *
 */
public enum Pickpocket {
	MAN(385,1, 8, new Item[] {new Item(995, 5) }),
	FARMER(3257,60, 65, new Item[] {new Item(5291), new Item(5292), new Item(5293), new Item(5294), new Item(5291), new Item(5292), new Item(5293), new Item(5294), new Item(5295), new Item(5296), new Item(5297), new Item(5298), new Item(5299), new Item(5300), new Item(5301), new Item(5302), new Item(5303), new Item(5304)});

	private final int npcId;
	
	private final int levelRequirement;
	
	private final int experience;

	private final Item[] rewards;

	private Pickpocket(final int npcId,final int level, final int experience, final Item[] loot) {
		this.levelRequirement = level;
		this.experience = experience;
		this.rewards = loot;
		this.npcId = npcId;
	}
	
	/**
	 * Data of our stuff.
	 */
	public static Map<Integer,Pickpocket> DATA = new HashMap<Integer,Pickpocket>();
	
	/**
	 * Staticly started on startup to add our pickpocket data.
	 */
	static {
		for (Pickpocket pp : values()) {
			DATA.put(pp.getNpcId(), pp);
		}
	}
	
	/**
	 * Get a pickpocket for a certain npc id.
	 * @param id the npcs id.
	 * @return the pickpocket from the data map.
	 */
	public static Pickpocket forId(int id) {
		return DATA.get(id);
	}
	
	public int getRequirement() {
		return levelRequirement;
	}

	public int getExperience() {
		return experience;
	}

	public Item[] getLoot() {
		return rewards;
	}

	public int getNpcId() {
		return npcId;
	}
}