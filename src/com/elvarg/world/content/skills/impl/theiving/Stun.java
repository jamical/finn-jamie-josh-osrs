package com.elvarg.world.content.skills.impl.theiving;

import com.elvarg.engine.task.Task;
import com.elvarg.engine.task.TaskManager;
import com.elvarg.util.Misc;
import com.elvarg.util.RandomGen;
import com.elvarg.world.model.Animation;
import com.elvarg.world.model.Graphic;
import com.elvarg.world.model.GraphicHeight;
import com.elvarg.world.model.Priority;
import com.elvarg.world.model.Skill;
import com.elvarg.world.model.movement.MovementStatus;

/**
 * A theiving stun. Like when your knocked out and birds are flying over your head when you fail at theioving.
 * @author jamix77
 *
 */
public class Stun {
	
	/**
	 * Stun constructor
	 * @param theiving theiving instance.
	 */
	public Stun(Theiving theiving) {
		this.theiving = theiving;
	}

	/**
	 * Our theiving instance that this belongs to.
	 */
	private Theiving theiving;
	
	/**
	 * The birds graphic
	 */
	private static final Graphic GFX = new Graphic(254, GraphicHeight.HIGH, Priority.HIGH);
	
	/**
	 * The block animation
	 */
	private static final Animation BLOCK = new Animation(424,Priority.HIGH);
	
	/**
	 * The punching animation
	 */
	private static final Animation PUNCH = new Animation(422,Priority.HIGH);
	
	public void stun() {
		//Birds over head GFX
				theiving.getPlayer().performGraphic(GFX);
				//theiving.getPlayer() animates the block animation. I think it should be changed to whatever the theiving.getPlayer()s current one is.
				theiving.getPlayer().performAnimation(BLOCK);
				//theiving.getPlayer() not allowed to move.
				theiving.getPlayer().getMovementQueue().setMovementStatus(MovementStatus.DISABLED);
				//Force chat message.
				theiving.getNpc().setPositionToFace(theiving.getPlayer().getPosition());
				//punch dis dude in the face. Dont steal from me again.
				theiving.getNpc().performAnimation(PUNCH);
				//chat.
				theiving.getNpc().forceChat("What do you think you're doing?");
				
				/**
				 * Task that makes the theiving.getPlayer() unstunned and can move.
				 */
				Task unStunTask = new Task(4,theiving.getPlayer(),false) {

					@Override
					protected void execute() {
						//can move now
						theiving.getPlayer().getMovementQueue().setMovementStatus(MovementStatus.NONE);
						theiving.getPlayer().getCombat().addDamage(theiving.getPlayer(), Misc.inclusive(1, 3));
						stop();
						return;
					}
					
				};
				//starts the task
				TaskManager.submit(unStunTask);
	}
	
	/**
	 * Are we stunned?
	 * @return
	 */
	public boolean success() {
		RandomGen random = new RandomGen();
		double level = theiving.getPlayer().getSkillManager().getCurrentLevel(Skill.THIEVING);
		double req = theiving.getPick().getRequirement();
		double successChance = Math.ceil((level * 50 - req * 15) / req / 3 * 4);
		int roll = random.inclusive(99);
		if (random.inclusive(12) < 2) {
		    return false;
		}
		if (successChance >= roll) {
		    return true;
		}
		return false;
	}

}
