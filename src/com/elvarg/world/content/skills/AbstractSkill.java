package com.elvarg.world.content.skills;

import com.elvarg.engine.task.Task;
import com.elvarg.engine.task.TaskManager;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Node;

/**
 * An abstract skill
 * @author jamix77
 *
 */
public abstract class AbstractSkill extends Task {
	
	public AbstractSkill() {
		
	}

	/**
	 * Player using the skill handler.
	 */
	protected Player player;
	
	/**
	 * How many ticks have gone by.
	 */
	protected int ticks = 0;
	
	/**
	 * Running flag.
	 */
	protected boolean running = false;
	
	/**
	 * Arguments
	 */
	private Object[] args;
	
	/**
	 * Constructor
	 * @param player the player who is now owning this skill.
	 */
	public AbstractSkill(Player player) {
		this.setPlayer(player);
	}
	
	/**
	 * Start the skilling.
	 */
	public void start() {
		getPlayer().setAbstractSkill(this);
		running = true;
		super.setDelay(1);
		super.bind(getPlayer());
		TaskManager.submit(this);
	}
	
	/**
	 * End the skilling.
	 */
	public void stop() {
		getPlayer().setAbstractSkill(null);
		running = false;
		super.stop();
	}
	
	/**
	 * Handle our skilling.
	 */
	public abstract void handle();
	
	/**
	 * Interaction type
	 * @return the interaction type.
	 */
	public abstract InteractionType interactionType();
	
	/**
	 * Thats that trigger this event
	 * @return
	 */
	public abstract Node[][] nodes();
	
	/**
	 * Default handling method 
	 */
	public void defaultHandle() {
		if (running) {
			ticks++;
			handle();
		} else {
			stop();
		}
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object... args) {
		this.args = args;
	}

	@Override
	protected void execute() {
		defaultHandle();
	}

	public Player getPlayer() {
		return player;
	}

	public AbstractSkill setPlayer(Player player) {
		this.player = player;
		return this;
	}
	
	
	
}
