package com.elvarg.world.content.skills;

import com.elvarg.world.entity.impl.npc.NPC;
import com.elvarg.world.entity.impl.object.GameObject;
import com.elvarg.world.model.Item;
import com.elvarg.world.model.Node;

/**
 * An abstract skill checker
 * @author jamix77
 *
 */
public class AbstractSkillChecker {

	public static boolean check(AbstractSkill skill, Node...nodes) {
		switch (skill.interactionType()) {
		case ITEM_ON_ITEM:
			return itemOnItemCheck(skill,nodes);
		case ITEM_CLICK_1:
			return itemClickCheck(skill,nodes);
		case ITEM_ON_OBJECT:
			return itemOnObjectCheck(skill,nodes);
		case NPC_OPTION_1:
			return npcOptionOneCheck(skill,nodes);
		default:
			return false;
		
		}
	}
	
	public static boolean npcOptionOneCheck(AbstractSkill skill, Node...nodes) {
		NPC npc = (NPC)nodes[0];
		for (Node n : skill.nodes()[0]) {
			NPC npc1 = (NPC)n;
			if (npc.getId() == npc1.getId()) {
				if (skill.getPlayer().getAbstractSkill() == null) {
					skill.setArgs(nodes);
					skill.start();
					skill.getPlayer().getPacketSender().sendMessage("Using skill handler " + name(skill));
				}
				return true;
			}
		}
		return false;
	}
	
	public static boolean itemOnObjectCheck(AbstractSkill skill, Node...nodes) {
		Item i = (Item)nodes[0];
		GameObject g = (GameObject)nodes[1];
		for (Node n : skill.nodes()[0]) {
			Item i1 = (Item)n;
			if (i1.getId() == i.getId()) {
				for (Node nb : skill.nodes()[1]) {
					if (((GameObject)nb).getId() == g.getId()) {
						if (skill.getPlayer().getAbstractSkill() == null) {
							skill.setArgs(nodes);
							skill.start();
							skill.getPlayer().getPacketSender().sendMessage("Using skill handler  " + name(skill));
						}
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean itemClickCheck(AbstractSkill skill, Node...nodes) {
		Item i = (Item)nodes[0];
		for (Node n :skill.nodes()[0]) {
			Item i1 = (Item)n;
			if (i1.getId() == i.getId()) {
				if (skill.getPlayer().getAbstractSkill() == null) {
					skill.setArgs(nodes);
					skill.start();
					skill.getPlayer().getPacketSender().sendMessage("Using Skill Handler " + name(skill));
				}
				return true;
			}
		}
		return false;
	}
	
	public static String name(AbstractSkill sk) {
		return sk.getClass().getName();
	}
	
	public static boolean itemOnItemCheck(AbstractSkill skill, Node...nodes) {
		Item[] n = {(Item)nodes[0],(Item)nodes[1]};
		Item[] n1 = new Item[skill.nodes()[0].length];
		Item[] n2 = new Item[skill.nodes()[1].length];
		int total1 = 0;
		int total2 = 0;
		for (Node nod : skill.nodes()[0]) {
			n1[total1] = (Item) nod;
			total1++;
		}
		for (Node nod : skill.nodes()[1]) {
			n2[total2] = (Item) nod;
			total2++;
		}
		//FIXME
		//TODO
		for (Item i : n1) {
			for (Item i1 : n2) {
				if (n[0].getId() == i.getId()) {
					if (n[1].getId() == i1.getId()) {
						if (skill.getPlayer().getAbstractSkill() == null) {
							skill.start();
							skill.setArgs(n);
						}
						return true;
					}
				}
				else if (n[1].getId() == i.getId()) {
					if (n[0].getId() == i1.getId()) {
						if (skill.getPlayer().getAbstractSkill() == null) {
							skill.start();
							skill.setArgs(n);
						}
						return true;
					}
				}
			}
		}
		return false;
	}
	
}
