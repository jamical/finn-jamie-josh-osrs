package com.elvarg.world.content.skills;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Skill handlers class.
 * @author jamix77
 *
 */
public class SkillHandlers {

	
	public static final HashMap<String,AbstractSkill> skills = new HashMap<>();
	
	/**
	 * Init our handlers.
	 */
	public static void init() {
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/handlers")));
			String read;
			while ((read = reader.readLine()) != null) {
				Class c = Class.forName(read);
				if (c != null) {
					try {
						AbstractSkill newSkill = (AbstractSkill) c.newInstance();
						skills.put(read,newSkill);
					} catch (Throwable t) {
						t.printStackTrace();
						continue;
					}
					
					
				}
			}
			reader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
