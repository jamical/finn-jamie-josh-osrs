package com.elvarg.world.content.skills;
/**
 * Interaction types
 * @author jamix77
 *
 */
public enum InteractionType {
	
	/**
	 * An item on item usage event. TODO Make seperate usage event classes.
	 * Usage in the Object array. {@code{Used (as item),Used With (as item)}}
	 */
	ITEM_ON_ITEM,
	
	/**
	 * An item clicking event interaction type.
	 * Usage in the Object array. {@code{Item (as item)}}
	 */
	ITEM_CLICK_1,
	
	/**
	 * A Npc option event for the X option.
	 * Usage in the Object array. {@code{NPC (npc)}}.
	 */
	NPC_OPTION_1, 
	NPC_OPTION_2,
	NPC_OPTION_3,
	
	/**
	 * An item on object usage event.
	 * Usage in the Object array. {@code{Item (item), Object (GameObject)}}.
	 */
	ITEM_ON_OBJECT;
}
