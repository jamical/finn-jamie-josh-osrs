package com.elvarg.world.model;

import com.elvarg.world.entity.impl.npc.NPC;
import com.elvarg.world.entity.impl.object.GameObject;
import com.elvarg.world.entity.impl.player.Player;

/**
 * A node. Of Anything.
 * @author jamix77
 */
public class Node {

	/**
	 * What type of node is this? so we are able to cast without problems.
	 * @return
	 */
	public NodeType type() {
		if (this instanceof GameObject) {
			return NodeType.GameObject;
		}
		else if (this instanceof Item) {
			return NodeType.Item;
		}
		else if (this instanceof Player) {
			return NodeType.Player;
		}
		else if (this instanceof NPC) {
			return NodeType.Npc;
		}
		return NodeType.Null;
	}
	
	/**
	 * An enumeration of the node types.
	 * @author jamix77
	 *
	 */
	public static enum NodeType {
		GameObject,
		Item,
		Player,
		Npc,
		Null;
		
	}
	
}
