package com.elvarg.cache.impl.definitions;

import com.elvarg.world.model.Position;

public class DefaultSpawnDefinition {
	
	private int id;
	private Position position;
	
	public int getId() {
		return id;
	}
	
	public Position getPosition() {
		return position;
	}
}
