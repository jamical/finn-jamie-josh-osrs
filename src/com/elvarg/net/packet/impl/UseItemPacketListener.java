package com.elvarg.net.packet.impl;

import com.elvarg.net.packet.Packet;
import com.elvarg.net.packet.PacketConstants;
import com.elvarg.net.packet.PacketListener;
import com.elvarg.world.content.skills.AbstractSkill;
import com.elvarg.world.content.skills.AbstractSkillChecker;
import com.elvarg.world.content.skills.InteractionType;
import com.elvarg.world.content.skills.SkillHandlers;
import com.elvarg.world.entity.impl.object.GameObject;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Item;
import com.elvarg.world.model.Position;

/**
 * Anything to do with item interaction will be handled here.
 * @author jamix77
 *
 */
public class UseItemPacketListener implements PacketListener {

	@Override
	public void handleMessage(Player player, Packet packet) {
		switch (packet.getOpcode()) {
		case PacketConstants.ITEM_ON_ITEM:
			itemOnItem(player,packet);
			break;
		case PacketConstants.ITEM_ON_OBJECT:
			itemOnObject(player,packet);
			break;
		}
	}
	
	/**
	 * Item on object packet listener. When you use
	 * an item on a game object it will call opcode 192 and
	 * trigger this class and then redirect it to this method.
	 * @param player the player
	 * @param packet the packet
	 * 
	 * buf.putOpcode(192);
		buf.putShort(anInt1284);
		buf.putShort(id);
		buf.writeSignedBigEndian(val1);
		buf.writeUnsignedWordBigEndian(anInt1283);
		buf.writeSignedBigEndian(val2);
		buf.putShort(anInt1285);
	 * 
	 */
	public  void itemOnObject(Player player, Packet packet) {
		int interfaceId = packet.readShort();
		int objectId = packet.readShort();
		int objY = packet.readLEShortA();
		int itemSlot = packet.readLEShort();
		int objX = packet.readLEShortA();
		int itemId = packet.readShort();
		
		if (itemSlot < 0 || itemSlot > player.getInventory().capacity()) {
			return;
		}
		
		final Item item = player.getInventory().getItems()[itemSlot];
		if (item == null || item.getId() != itemId) {
			return;
		}
		final GameObject object = new GameObject(objectId,new Position(objX,objY,player.getPosition().getZ()));
		if (object.getId() != objectId) {
			return;
		}
		//items objects
		for (AbstractSkill s : SkillHandlers.skills.values()) {
			if (s.interactionType() != InteractionType.ITEM_ON_OBJECT) {
				continue;
			}
			try {
				if (AbstractSkillChecker.check(((AbstractSkill)s.getClass().newInstance()).setPlayer(player), item,object)) {
					return;
				}
			} catch (InstantiationException | IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void itemOnItem(Player player, Packet packet) {
		int usedWithSlot = packet.readUnsignedShort();
		int itemUsedSlot = packet.readUnsignedShortA();
		if (usedWithSlot <0 || itemUsedSlot < 0 || itemUsedSlot > player.getInventory().capacity() || usedWithSlot > player.getInventory().capacity()) {
			return;
		}
		Item used = player.getInventory().getItems()[itemUsedSlot];
		Item usedWith = player.getInventory().getItems()[usedWithSlot];
		player.getPacketSender().sendInterfaceRemoval();
		for (AbstractSkill s : SkillHandlers.skills.values()) {
			try {
				if (s.interactionType() != InteractionType.ITEM_ON_ITEM) {
					continue;
				}
				if (AbstractSkillChecker.check(((AbstractSkill)s.getClass().newInstance()).setPlayer(player), used,usedWith)) {
					return;
				}
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		
	}
	

}
