package com.elvarg;

import com.elvarg.world.model.Position;

public class GameConstants {

    public static final boolean IS_DEADMAN_ENABLED = false;
    public static int WORLD_ID = (IS_DEADMAN_ENABLED ? 2 : 1);
    public static boolean IS_DOUBLE_XP = false;
    public static boolean MySQL_ENABLED = true;
    public static final int GAME_VERSION = 4;
    public static final int GAME_UID = 2;
    public static final String DEFINITIONS_DIRECTORY = "./data/definitions/";
    public static final String CLIPPING_DIRECTORY = "./data/clipping/";
    public static final String DATA_DIR = "./Data2/";
    public static final boolean CONCURRENCY = Runtime.getRuntime().availableProcessors() > 1;
    public static final int GAME_ENGINE_PROCESSING_CYCLE_RATE = 600;
    public static final int QUEUED_LOOP_THRESHOLD = 45;
    public static final Position DEFAULT_POSITION = new Position(2605, 3088);
    public static final String DEFAULT_CLAN_CHAT = "osmayhem";
    public static final boolean QUEUE_SWITCHING_REFRESH = false;
    public static final int DROP_THRESHOLD = 2;
    public static final double EXP_MULTIPLIER = 12.3D;
    public static final boolean JAGGRAB_ENABLED = true;
    public static final int[] TAB_INTERFACES = {2423, 3917, 44000, 3213, 1644, 5608, 37128, 2449, 5065, 5715, 2449, 42500, 147, 32000};

    public static boolean isIsDoubleXp() {
        return IS_DOUBLE_XP;
    }

    public static void setIsDoubleXp(boolean isDoubleXp) {
        IS_DOUBLE_XP = isDoubleXp;
    }

<<<<<<< HEAD
=======
	/**
	 * Is JAGGRAB enabled?
	 */
	public static final boolean JAGGRAB_ENABLED = true;
	
	/**
	 * What is the multiplier of osrs xp?
	 */
	public static final int MULTIPLIER = 6;
	
	/**
	 * The game engine rate
	 */
	public static final int GAME_ENGINE_PROCESSING_CYCLE_RATE = 300;

	/**
	 * The maximum amount of iterations that should occur per queue.
	 */
	public static final int QUEUED_LOOP_THRESHOLD = 50;

	/**
	 * The game version
	 */
	public static final int GAME_VERSION = 1;

	/**
	 * The secure game UID /Unique Identifier/ 
	 */
	public static final int GAME_UID = 23;

	/**
	 *  The default position in game.
	 */
	public static final Position DEFAULT_POSITION = new Position(3091, 3503);

	/**
	 * Should the inventory be refreshed immediately
	 * on switching items or should it be delayed
	 * until next game cycle?
	 */
	public static final boolean QUEUE_SWITCHING_REFRESH = true;

	/**
	 * The tab interfaces in game.
	 * {Gameframe}
	 * [0] = tab Id, [1] = tab interface Id
	 */
	public static final int TAB_INTERFACES[][] =
		{
				{0, 2423}, {1, 3917}, {2, 639}, {3, 3213}, {4, 1644}, {5, 5608}, {6, -1}, //Row 1

				{7, 37128}, {8, 5065}, {9, 5715}, {10, 2449}, {11, 42500}, {12, 147}
		};
>>>>>>> f35fff9218c6ba337b7a2e5884285932388c6263
}
