package com.elvarg.game.definition.loader.impl;

import java.util.logging.Logger;

import fileserver.FileServerConstants;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.util.ResourceLeakDetector;
import io.netty.util.ResourceLeakDetector.Level;

/**
 * The main class of the FileServer.
 * Services are started here..
 *
 * @author Professor Oak
 *
 * Credits to:
 * @author Graham
 * @author Nikki
 * For references from their update-server in the
 * Apollo source.
 */
public final class FileServer {

    /**
     * The logger for this
     * lass.
     */
    private static final Logger logger = Logger.getLogger(FileServer.class.getName());

    private static CacheLoader cacheLoader = new CacheLoader();

    public static final CacheLoader getCacheLoader() {
        return cacheLoader;
    }

    /**
     * Starts the FileServer system.
     * @throws Exception 	if an error occurs.
     */
    public static final void init() throws Exception {

        logger.info("Starting File-Server service on port "+FileServerConstants.FILE_SERVER_PORT+"..");
        getCacheLoader().init();

        ResourceLeakDetector.setLevel(Level.ADVANCED);
        EventLoopGroup loopGroup = new NioEventLoopGroup();
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(loopGroup).channel(NioServerSocketChannel.class)
                .childHandler(new PipelineFactory()).bind(FileServerConstants.FILE_SERVER_PORT).syncUninterruptibly();

        logger.info("File-Server has been initialised on port "+FileServerConstants.FILE_SERVER_PORT+".");
    }
}