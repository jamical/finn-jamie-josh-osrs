package handlers.skills.theiving;

import com.elvarg.world.content.handlers.OptionHandler;
import com.elvarg.world.content.skills.impl.theiving.Pickpocket;
import com.elvarg.world.content.skills.impl.theiving.Theiving;
import com.elvarg.world.entity.impl.npc.NPC;
import com.elvarg.world.entity.impl.player.Player;
import com.elvarg.world.model.Node;
import com.elvarg.world.model.Node.NodeType;

/**
 * A theiving npc option handler.
 * @author jamix77
 *
 */
public class TheivingOptionHandler extends OptionHandler {

	@Override
	public void initAssign() {
		assign(new NPC(3257,null), 2, this);
	}

	@Override
	public void handle(Player player,Node node) {
		if (node.type() == NodeType.Npc) {
			if (player.getAbstractSkill() == null) {
				Theiving theiving = new Theiving(player);
				theiving.setPick(Pickpocket.forId(((NPC)node).getId()));
				theiving.setNpc((NPC)node);
				theiving.start();
			}
		}
	}

}
